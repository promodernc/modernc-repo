#pragma once
#include "SFML\Graphics.hpp"

enum button_states{BTN_IDLE = 0, BTN_HOVER, BTN_PRESSED};

class Button
{
public:
	Button(sf::Vector2f coordinates, float width, float height,
		sf::Font font, std::string text,
		sf::Color idleColor, sf::Color hoverColor, sf::Color activeColor);
	~Button();
	void Update(const sf::Vector2f mousePos);
	void Draw(sf::RenderWindow& window);
	void Hide();
	void Show();
	bool IsPressed();


private:

	float initialX;
	float initialY;

	bool clickable;

	short unsigned buttonState;

	sf::RectangleShape shape;
	sf::Font font;
	sf::Text text;

	sf::Color idleColor;
	sf::Color hoverColor;
	sf::Color activeColor;


};

