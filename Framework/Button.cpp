#include "Button.h"
Button::Button(sf::Vector2f coordinates, float width, float height,
	sf::Font font, std::string text,
	sf::Color idleColor, sf::Color hoverColor, sf::Color activeColor)

{
	this->clickable = false;

	this->shape.setPosition(coordinates);
	this->shape.setSize(sf::Vector2f(width, height));

	this->font = font;
	this->text.setFont(this->font);
	this->text.setString(text);
	this->text.setFillColor(sf::Color::Black);
	this->text.setCharacterSize(12);
	this->text.setPosition(sf::Vector2f(this->shape.getPosition().x + (this->shape.getLocalBounds().width - this->text.getGlobalBounds().width) / 2,
		this->shape.getPosition().y + (this->shape.getLocalBounds().height - this->text.getGlobalBounds().height) / 2));

	this->idleColor = idleColor;
	this->hoverColor = hoverColor;
	this->activeColor = activeColor;

	this->shape.setFillColor(this->idleColor);
	this->buttonState = BTN_IDLE;

	this->initialX = coordinates.x;
	this->initialY = coordinates.y;
}
Button::~Button()
{

}
void Button::Update(const sf::Vector2f mousePos)
{
		//IDLE
		this->buttonState = BTN_IDLE;

		// HOVER
		if (this->shape.getGlobalBounds().contains(mousePos))
		{
			this->buttonState = BTN_HOVER;

			//PRESSED
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				this->buttonState = BTN_PRESSED;
			}
		}

		switch (buttonState) {
		case BTN_IDLE:

			this->shape.setFillColor(idleColor);
			break;

		case BTN_HOVER:

			this->shape.setFillColor(hoverColor);
			break;

		case BTN_PRESSED:

			this->shape.setFillColor(activeColor);
			break;

		default:
			break;
		}
}

void Button::Hide()
{
	this->shape.setPosition(sf::Vector2f(2000, 2000));
	this->text.setPosition(sf::Vector2f(2000, 2000));
}

void Button::Show()
{
	this->shape.setPosition(sf::Vector2f(initialX, initialY));
	this->text.setPosition(sf::Vector2f(initialX + (this->shape.getLocalBounds().width - this->text.getGlobalBounds().width) / 2,
		initialY + (this->shape.getLocalBounds().height - this->text.getGlobalBounds().height) / 2));
}

bool Button::IsPressed()
{
	if (buttonState == BTN_PRESSED)
		return true;
	return false;
}

void Button::Draw(sf::RenderWindow& window)
{
	this->clickable = true;
	window.draw(this->shape);
	window.draw(this->text);
}
