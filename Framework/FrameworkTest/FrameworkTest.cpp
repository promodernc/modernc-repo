#include "pch.h"
#include "CppUnitTest.h"
#include"../Framework/Ball.h"
#include"../Framework/Brick.h"
#include <SFML/Graphics.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace FrameworkTest
{
	TEST_CLASS(FrameworkTest)
	{
	public:
		
		TEST_METHOD(Constructortest)
		{
			sf::Vector2f size(50, 50);
			Brick bricktest(size);
			Assert::IsTrue(bricktest.GetSize()==size);
		}

		TEST_METHOD(SetTest)
		{
			Brick bricktest(sf::Vector2f(50, 50));
			bricktest.SetPosition(sf::Vector2f(250,300));
			Assert::IsTrue(bricktest.GetX() == 250);
			Assert::IsTrue(bricktest.GetY() == 300);

		}
	};
}
