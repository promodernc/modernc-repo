#include "pch.h"
#include "CppUnitTest.h"
#include"../Framework/Ball.h"
#include"../Framework/Brick.h"
#include <SFML/Graphics.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace FrameworkTest
{
	TEST_CLASS(BallTest)
	{
	public:

		TEST_METHOD(Constructortest)
		{
			sf::Vector2f position(500, 500);
			Ball balltest;
			Assert::IsTrue(balltest.GetRadius()==12);
			Assert::IsTrue(balltest.GetPosition() == position);
		}

		TEST_METHOD(Movingtest)
		{
			Ball ball;
			ball.Move();
			Assert::IsTrue(ball.IsMoving() ==true);
			ball.Stop();
			Assert::IsTrue(ball.IsMoving() == false);

		}
	};
}