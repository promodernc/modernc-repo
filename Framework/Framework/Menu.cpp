#include "Menu.h"
#include "..\Button.h"
#include<sstream>


Menu::Menu(float width, float height)
{
	if (!font.loadFromFile("arial.ttf"))
	{
		// handle error
	}
	//text.setFont(font);
	//text.setCharacterSize(30);
	//text.setPosition({ 400,850 });
	//text.setFillColor(sf::Color::Magenta);
	////textcontent << "Framework";
	//text.setString(textcontent.str());

	this->play_brick_button = new Button(sf::Vector2f(width / 2 - 75, height / (3 + 1) * 1), 150, 90, font, "Play Brick Breaker", sf::Color::Green, sf::Color(255, 222, 173), sf::Color::Red);
	this->play_pong_sp_button = new Button(sf::Vector2f(width / 2 - 200, height / (3 + 1) * 2), 150, 90, font, "Play Pong SinglePlayer", sf::Color::Green, sf::Color(255, 222, 173), sf::Color::Red);
	this->play_pong_mp_button = new Button(sf::Vector2f(width / 2 + 50, height / (3 + 1) * 2), 150, 90, font, "Play Pong MultiPlayer", sf::Color::Green, sf::Color(255, 222, 173), sf::Color::Red);
	this->exit_button = new Button(sf::Vector2f(width / 2 - 75, height / (3 + 1) * 3), 150, 90, font, "Exit", sf::Color::Green, sf::Color(255, 222, 173), sf::Color::Red);

	
}


Menu::~Menu()
{
}


void Menu::Draw(sf::RenderWindow& window)
{
	
	this->play_brick_button->Draw(window);
	this->play_pong_sp_button->Draw(window);
	this->play_pong_mp_button->Draw(window);
	this->exit_button->Draw(window);
	
}

void Menu::update(sf::Vector2f mousePos)
{
	int mouse_x = mousePos.x;
	int mouse_y = mousePos.y;
	this->play_brick_button->Update(sf::Vector2f(mouse_x, mouse_y));
	this->play_pong_sp_button->Update(sf::Vector2f(mouse_x, mouse_y)); 
	this->play_pong_mp_button->Update(sf::Vector2f(mouse_x, mouse_y));
	this->exit_button->Update(sf::Vector2f(mouse_x, mouse_y));
}

int Menu::GetPressedItem()
{
	if (this->play_brick_button->IsPressed())
		return 0;
	if (this->play_pong_sp_button->IsPressed())
		return 1;
	if (this->play_pong_mp_button->IsPressed())
		return 2;
	if (this->exit_button->IsPressed())
		return 3;
	return -1;
}

void Menu::UpdateMousePositions(sf::RenderWindow& window, sf::Vector2f& mousePos)
{
	this->mouse_pos_screen = sf::Mouse::getPosition();
	this->mouse_pos_window = sf::Mouse::getPosition(window);
	this->mouse_pos_view = window.mapPixelToCoords(sf::Mouse::getPosition(window));
	mousePos = mouse_pos_view;
}
//void Menu::MoveUp()
//{
//	if (selectedItemIndex - 1 >= 0)
//	{
//		menu[selectedItemIndex].setFillColor(sf::Color::White);
//		selectedItemIndex--;
//		menu[selectedItemIndex].setFillColor(sf::Color::Cyan);
//	}
//}
//
//void Menu::MoveDown()
//{
//	if (selectedItemIndex + 1 < 3)
//	{
//		menu[selectedItemIndex].setFillColor(sf::Color::White);
//		selectedItemIndex++;
//		menu[selectedItemIndex].setFillColor(sf::Color::Cyan);
//	}
//}