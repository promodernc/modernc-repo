#pragma once
#include "MyGame.h"
#include "Ball.h"
#include "Paddle.h"
#include "Brick.h"


class BrickGame:public MyGame
{
public:
	 BrickGame();
	 void SetObjectAndPlayer() override;
	 void SetGameConditions() override;
	 void ResetGame(sf::RenderWindow &window, std::vector<Ball>& ball);
	 void RemoveBricks();
	 int lives;
	 bool score_saved;
private:
	std::vector<Brick*> bricks;
	Brick* brick;
	int nrbrick;
	int gameover;

};

