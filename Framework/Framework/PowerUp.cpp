#include "PowerUp.h"

void PowerUp::SizeUp(Paddle& paddle)
{
	paddle.SetSize(sf::Vector2f(0, paddle.GetHeight() + 10));
}

void PowerUp::SizeDown(Paddle& paddle)
{
	paddle.SetSize(sf::Vector2f(0, paddle.GetHeight() - 10));
}

void PowerUp::SizeUpBrickBreaker(Paddle& paddle)
{
	paddle.SetSize(sf::Vector2f(paddle.GetWidth() + 10, 0));
}

void PowerUp::SizeDownBrickBreaker(Paddle& paddle)
{
	paddle.SetSize(sf::Vector2f(paddle.GetWidth()-10, 0));
}

void PowerUp::SpeedUpPaddle(Paddle& Paddle)
{
	Paddle.SetSpeed(Paddle.GetSpeed()+0.1);
}

void PowerUp::SpeedUp(Ball& ball)
{
	ball.SetSpeed(ball.GetSpeed() + 4);
}

void PowerUp::SpeedDown(Ball& ball)
{
	ball.SetSpeed(ball.GetSpeed() - 4);
}

void PowerUp::ChangeBrickToBall(std::vector<Ball>& vector, Ball& ball,Brick* brick, sf::RenderWindow& window)
{
	ball.SetPosition(sf::Vector2f(brick->GetX(), brick->GetY()));
	vector.push_back(ball);
	window.display();
}

void PowerUp::DrawPowerUpSpeed(sf::CircleShape& power, Brick* brick, sf::RenderWindow& window)
{
	
	
	window.draw(power);
	
}
