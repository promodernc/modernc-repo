#include "Brick.h"
#include "Ball.h"
#include"Player.h"
#include<vector>
#include <random>
#include <time.h>
void Brick::draw(sf::RenderWindow& window)
{	
	window.draw(brick);
}

bool Brick::OkToAddBrick(Brick* b, vector<Brick*> bricks)
{
	for (Brick* brick : bricks)
	{
		if ((brick->GetX() >= b->GetX() - (b->GetSize().x + 5) && brick->GetX() <= b->GetX() + (b->GetSize().x + 5))
			&& brick->GetY() >= b->GetY() - (b->GetSize().y + 5) && brick->GetY() <= b->GetY() + (b->GetSize().y + 5))
			return false;
	}
	return true;
}

void Brick::GenerateBricks(vector<Brick*>& bricks, int nr,std::string game)
{
	std::vector<sf::Color>asd;
	asd.push_back(sf::Color::Red);
	asd.push_back(sf::Color::Cyan);
	srand(time(NULL));
	for (int i = 0; i < nr; i++)
	{
		if (game == "Pong")
		{
			float x = rand() % 220 + 300;
			float y = rand() % 835 + 10;
			Brick* b = new Brick(sf::Vector2f(x, y));
			b->SetColor(sf::Color::Red);
			while (!OkToAddBrick(b, bricks))
			{
				delete b;
				x = rand() % 220 + 300;
				y = rand() % 835 + 10;
				b = new Brick(sf::Vector2f(x, y));
				b->SetColor(sf::Color::Cyan); 
				
			}
			bricks.push_back(b);
		}
		else if (game == "Brick")
		{
			float x = rand() % 800+1;
			float y = rand() % 350+1;
			Brick* b = new Brick(sf::Vector2f(x, y));
			b->SetColor(sf::Color(asd.at(rand()%2)));
			while (!OkToAddBrick(b, bricks))
			{
				delete b;
				x = rand() %800 + 1;
				y = rand() %350 + 1;
				b = new Brick(sf::Vector2f(x, y));
				b->SetColor(sf::Color(asd.at(rand() % 2)));
			}
			bricks.push_back(b);

		}
	}
}

void Brick::RemoveBricks(vector<Brick*>& bricks)
{
	for (Brick* b : bricks)
		b->SetPosition(sf::Vector2f(2000, 2000));
	bricks.empty();
}

void Brick::DrawBricks(sf::RenderWindow& window, vector<Brick*> bricks)
{
	for (Brick* brick : bricks)
		brick->draw(window);
}

Brick::Brick(sf::Vector2f pozitie)
{
	brick.setSize(sf::Vector2f(55, 55));
	brick.setFillColor(sf::Color::White);
	brick.setPosition(pozitie);
}

float Brick::GetX() const
{
	return brick.getPosition().x;
}

float Brick::GetY() const
{
	return brick.getPosition().y;
}

sf::Vector2f Brick::GetSize() const
{
	return brick.getSize();
}

void Brick::SetPosition(sf::Vector2f pozition)
{
	brick.setPosition(pozition);
}

void Brick::SetColor(sf::Color color)
{
	brick.setFillColor(color);
}

CollisionSide Brick::CheckBallCollision(Ball ball)
{
	//if (ball.GetPosition().x - ball.GetRadius() < brick.getPosition().x + brick.getLocalBounds().width
	//	&& ball.GetPosition().x + ball.GetRadius() > brick.getPosition().x 
	//	&&(ball.GetPosition().y - ball.GetRadius() < brick.getPosition().y + brick.getLocalBounds().height
	//	&& ball.GetPosition().y + ball.GetRadius() > brick.getPosition().y))
	//{
	//	return true;
	//}
	if ((ball.GetPosition().x - ball.GetRadius() <= brick.getPosition().x + brick.getLocalBounds().width // Coliziune din dreapta
		&& ball.GetPosition().x - ball.GetRadius() > brick.getPosition().x
		&& ball.GetPosition().y >= brick.getPosition().y
		&& ball.GetPosition().y <= brick.getPosition().y + brick.getLocalBounds().height)
		|| ((ball.GetPosition().x + ball.GetRadius() >= brick.getPosition().x // Coliziune din stanga
		&& ball.GetPosition().x + ball.GetRadius() < brick.getPosition().x + brick.getLocalBounds().width
		&& ball.GetPosition().y >= brick.getPosition().y
		&& ball.GetPosition().y <= brick.getPosition().y + brick.getLocalBounds().height)))
		return CollisionSide::LeftAndRightSide;
	if ((ball.GetPosition().y + ball.GetRadius() > brick.getPosition().y // Coliziune sus
		&& ball.GetPosition().y + ball.GetRadius() < brick.getPosition().y + brick.getLocalBounds().height
		&& ball.GetPosition().x >= brick.getPosition().x
		&& ball.GetPosition().x <= brick.getPosition().x + brick.getLocalBounds().width)
		|| ((ball.GetPosition().y - ball.GetRadius() < brick.getPosition().y // Coliziune jos
		&& ball.GetPosition().y - ball.GetRadius() > brick.getPosition().y + brick.getLocalBounds().height
		&& ball.GetPosition().x >= brick.getPosition().x
		&& ball.GetPosition().x <= brick.getPosition().x + brick.getLocalBounds().width)))
		return CollisionSide::BottomAndTopSide;
}