#pragma once
#include<SFML/Graphics.hpp>
#include"Ball.h"
#include"StateType.h"

class Paddle
{
public:
	Paddle();
	Paddle(sf::Vector2f position, sf::Keyboard::Key move_up, sf::Keyboard::Key move_down, sf::Keyboard::Key move_left, sf::Keyboard::Key move_right);
	void Move(sf::Vector2f dir);
	void SetSpeed(float speed);

	float GetSpeed() const;
	float GetX() const;
	float GetY() const;
	uint16_t GetWidth() const;
	uint16_t GetHeight() const;

	void SetSize(sf::Vector2f size);
	StateType CheckBallLocation(Ball ball);
	void SelfMove(Ball ball);
	void MovePaddle(std::string);
	StateType CheckWallCollision(sf::RenderWindow& window);
	bool CheckPongBallCollisionPlayer(Ball &ball, sf::Vector2f &new_pos, sf::Vector2f &direction);
	bool CheckPongBallCollisionEnemy(Ball &ball, sf::Vector2f &new_pos, sf::Vector2f &direction);
	StateType CheckBrickBreakerBallCollision(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction);

	void FixWallCollision(sf::RenderWindow& window);
	void FixPongBallCollisionPlayer(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction);
	void FixPongBallCollisionEnemy(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction);
	void FixBrickBreakerBallCollision(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction);
	void Draw(sf::RenderWindow& window);
	void SetThiccness(int);

	void SetPosition(sf::Vector2f position);

private:
	sf::RectangleShape paddle;
	float movespeed;
	sf::Keyboard::Key move_up, move_down, move_left, move_right;

};


