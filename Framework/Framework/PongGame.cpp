#include"PongGame.h"
#include"Ball.h"
#include"Brick.h"
#include"Paddle.h"
#include"Player.h"
#include"PowerUp.h"
#include"..\Button.h"

#include<fstream>
#include<ctime>
#include<iostream>
#include<sstream>
#include<random>
#include<vector>
#include<thread>
#include<chrono>
#include<mutex>


PongGame::PongGame(bool multiplayer)
{
	this->brick = new Brick(sf::Vector2f(20, 20));
	this->brick->SetColor(sf::Color(rand() % 255 + 1, rand() % 255 + 1, rand() % 255 + 1, 255));
	this->brick->GenerateBricks(bricks, 20, "Pong");
	this->nrbrick = 20;
	this->m_multiplayer = multiplayer;
}

void PongGame::RemoveBricks()
{
	this->brick->RemoveBricks(this->bricks);
}

void PongGame::ResetGame(sf::RenderWindow& window, std::vector<Ball>& ball, Paddle player1, Paddle player2)
{
	window.clear();
	this->brick->RemoveBricks(bricks);
	this->brick->GenerateBricks(bricks, 20, "Pong");
	this->nrbrick = 20;

	for (int index = 0; index < ball.size(); index++) {

		ball[index].SetColor(sf::Color::Yellow);
		ball[index].SetThiccness(3);
		ball[index].SetSpeed(15);
		ball[index].SetPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));
		ball[index].Move();
	}
	player1.SetThiccness(3);
	
	player2.SetThiccness(3);
	player1.SetPosition(sf::Vector2f(50, 450));
	player2.SetPosition(sf::Vector2f(850, 450));
}

//void ResetGame(sf::RenderWindow& window, std::vector<Ball>& ball)
//{
//	window.clear();
//	for (int index = 0; index < ball.size(); index++) {
//		ball[index].Show();
//		ball[index].SetSpeed(15);
//		ball[index].SetPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));
//	}
//}

void PongGame::SetObjectAndPlayer()
{
	srand(time(NULL));
	sf::RenderWindow window;
	sf::Vector2i centerWindow((sf::VideoMode::getDesktopMode().width / 2) - 445, (sf::VideoMode::getDesktopMode().height / 2) - 480);
	window.create(sf::VideoMode(900, 900), "Pong", sf::Style::Titlebar | sf::Style::Close);
	window.setPosition(centerWindow);
	window.setKeyRepeatEnabled(true);

	//***Defining Objects***//
	//file
	ofstream MyFile;

	//Time 

	time_t now = time(0);
	time_t result = time(NULL);
	char dt[26];
	ctime_s(dt, sizeof dt, &result);

	//Mouse

	sf::Mouse mouse;
	sf::Vector2f mouse_position_window;

	//Paddles

	Paddle player_paddle(sf::Vector2f(50, 450), sf::Keyboard::W, sf::Keyboard::S, sf::Keyboard::A, sf::Keyboard::D);
	Paddle enemy_paddle(sf::Vector2f(850, 450), sf::Keyboard::Up, sf::Keyboard::Down, sf::Keyboard::Left, sf::Keyboard::Right);
	player_paddle.SetSize(sf::Vector2f(0, 100));
	player_paddle.SetSpeed(0.9);
	enemy_paddle.SetSize(sf::Vector2f(0, 100));
	enemy_paddle.SetSpeed(0.9);

	//Powerups

	PowerUp p;

	//line in the middle

	sf::Vertex line2[] =
	{
		sf::Vertex(sf::Vector2f(450, 0)),
		sf::Vertex(sf::Vector2f(450, 900))
	};

	//ball

	std::vector<Ball> ball;
	Ball first;
	ball.push_back(first);

	// Declaring Players

	Player player1("Player1", 1, 1, 1);
	Player player2("Player2", 1, 2, 3);
	player1.SetScorePosition(sf::Vector2f(0, 800));
	player2.SetScorePosition(sf::Vector2f(800, 800));

	//Setting values, for moving the ball

	std::vector<sf::Vector2f> direction;
	sf::Vector2f dir(15, 5);
	direction.push_back(dir);
	sf::Clock clock;
	sf::Time elapsed = clock.restart();
	const sf::Time update_ms = sf::seconds(1.f / 90.f);

	//font reading

	sf::Font agency;
	agency.loadFromFile("AGENCYB.ttf");
	sf::Font arial;
	arial.loadFromFile("arial.ttf");

	Button* try_again_button = new Button(sf::Vector2f(window.getSize().x / 2 - 75, (window.getSize().y / 4) * 3), 150, 90, arial, "Try again", sf::Color::Green, sf::Color::White, sf::Color::Red);

	//starting of program

	while (window.isOpen())
	{
		// Optimize mouse position for window
		mouse_position_window = window.mapPixelToCoords(sf::Mouse::getPosition(window));
		try_again_button->Update(mouse_position_window);

		//Event Loop:

		sf::Event Event;

		sf::Text GameMessage, Winner;
		GameMessage.setCharacterSize(100);
		GameMessage.setPosition({ 250,250 });
		GameMessage.setFont(agency);
		GameMessage.setFillColor(sf::Color::Red);

		Winner.setCharacterSize(100);
		Winner.setPosition({ 250,450 });
		Winner.setFont(agency);
		Winner.setFillColor(sf::Color::Red);

		sf::Text scoring_text;
		std::ostringstream text;
		scoring_text.setCharacterSize(30);
		scoring_text.setPosition({ 360, 550 });
		scoring_text.setFont(arial);
		scoring_text.setFillColor(sf::Color::Red);

		bool player1_scored = false;
		bool player2_scored = false;

		while (window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				window.close();
			}
		}
		for (int index = 0; index < ball.size(); index++)
		{
			if (ball[index].IsMoving())
			{
				auto velocity = ball[index].GetSpeed();
				elapsed += clock.restart();
				while (elapsed >= update_ms) {
					const auto pos = ball[index].GetPosition();
					const auto delta = update_ms.asSeconds() * velocity;
					sf::Vector2f new_pos(pos.x + direction[index].x * delta, pos.y + direction[index].y * delta);
					if (ball[index].IsMoving())
					{
						if (new_pos.x - ball[index].GetRadius() < 0) { // left window edge
							//ball[index].Stop();
							direction[index].x *= -1;
							player2.IncreaseScore();

							text << "Player 2 Scored!";
							player2_scored = true;
							scoring_text.setString(text.str());
							window.draw(scoring_text);
							window.display();

							/*std::once_flag flag;
							std::call_once(flag, []() {
								std::chrono::seconds timespan(2);
								std::this_thread::sleep_for(timespan); 
								});*/

							text.str("");
							//ball[index].SetSpeed(15);
							scoring_text.setString(text.str());
						}

						else if (new_pos.x + ball[index].GetRadius() >= window.getSize().x) { // right window edge
							//ball[index].Stop();
							direction[index].x *= -1;
							new_pos.x = window.getSize().x - ball[index].GetRadius();
							player1.IncreaseScore();

							text << "Player 1 Scored!";
							player1_scored = true;
							scoring_text.setString(text.str());
							window.draw(scoring_text);
							window.display();

							/*std::once_flag flag;
							std::call_once(flag, []() {
								std::chrono::seconds timespan(2);
								std::this_thread::sleep_for(timespan); 
								});*/

							text.str("");
							scoring_text.setString(text.str());

							//ball[index].SetSpeed(15);
						}

						else if (new_pos.y - ball[index].GetRadius() < 0) { // top of window
							direction[index].y *= -1;
							new_pos.y = 0 + ball[index].GetRadius();
						}

						else if (new_pos.y + ball[index].GetRadius() >= window.getSize().y) { // bottom of window
							direction[index].y *= -1;
							new_pos.y = window.getSize().y - ball[index].GetRadius();
						}
						ball[index].Move();
					}

					if (player1_scored)
					{
						ball[index].SetPosition(sf::Vector2f(window.getSize().x / 4 * 3, window.getSize().y / 2));
						//new_pos.x = 0 + (window.getSize().x / 4) * 3;
						//new_pos.y = 0 + (window.getSize().y / 2);
						player1_scored = false;
					}
					if (player2_scored)
					{
						ball[index].SetPosition(sf::Vector2f(window.getSize().x / 4, window.getSize().y / 2));
						//new_pos.x = 0 + (window.getSize().x / 4);
						//new_pos.y = 0 + (window.getSize().y / 2);
						player2_scored = false;
					}

					player_paddle.FixPongBallCollisionPlayer(ball[index], new_pos, direction[index]);
					enemy_paddle.FixPongBallCollisionEnemy(ball[index], new_pos, direction[index]);

					ball[index].SetPosition(new_pos);
					ball[index].Draw(window);
					window.display();
					elapsed -= update_ms;
				}

			}

			window.clear(sf::Color(0, 0, 0));
			window.draw(player1.GetScore());
			window.draw(player2.GetScore());
			window.draw(line2, 2, sf::Lines);

			this->brick->DrawBricks(window, this->bricks);

			for (Brick* b : bricks)
			{
				if (b->CheckBallCollision(ball[index]) == CollisionSide::LeftAndRightSide)
				{
					direction[index].x *= -1;
					int powerup = 0;
					srand(time(NULL));
					powerup = rand() % 100 + 1;
					if (powerup % 2 == 0)
					{
						p.SizeUp(player_paddle);
						p.SizeUp(enemy_paddle);
					}

					if (powerup % 2 == 1)
					{
						p.SpeedUp(ball[index]);
					}

					if (powerup % 100 == 0)
					{
						p.ChangeBrickToBall(ball, first, b, window);
						direction.push_back(dir);
					}
					b->SetPosition(sf::Vector2f(901, 901));
				}
				else if (b->CheckBallCollision(ball[index]) == CollisionSide::BottomAndTopSide)
				{
					direction[index].y *= -1;
					int powerup = 0;
					srand(time(NULL));
					powerup = rand() % 100 + 1;
					if (powerup % 2 == 0)
					{
						p.SizeDown(player_paddle);
						p.SizeDown(enemy_paddle);
					}

					if (powerup % 2 == 1)
						p.SpeedDown(ball[index]);

					if (powerup % 100 == 0)
					{
						p.ChangeBrickToBall(ball, first, b, window);
						direction.push_back(dir);
					}
					b->SetPosition(sf::Vector2f(901, 901));
				}
			}


			std::ostringstream ssScore, sssScore;
			if (player1.GetPoint() == 5 || player2.GetPoint() == 5)
			{
				ball[index].SetColor(sf::Color::Black);
				ball[index].SetThiccness(0);
				ball[index].SetSpeed(0);
				ball[index].SetPosition(sf::Vector2f(-100, -100));
				
				ssScore << "GAME OVER ";
				GameMessage.setString(ssScore.str());
				window.clear(sf::Color::Black);
				window.draw(GameMessage);
				try_again_button->Show();
				try_again_button->Draw(window);
			}

			if (player1.GetPoint() == 7)
			{
				if (this->score_saved == false)
				{
					MyFile.open("PongPoint", ios::app);
					MyFile << player1.GetName() << "has points: " << player1.GetPoint() << " Won at time :" << dt << "" << endl;
					MyFile.close();
					this->score_saved = true;
				}
				ball[index].Stop();
				window.clear(sf::Color(0, 0, 0));
				GameMessage.setFillColor(sf::Color::Black);
				sssScore << "Player 1 WON";
				Winner.setString(sssScore.str());
				window.draw(Winner);

				try_again_button->Show();
				try_again_button->Draw(window);
			}

			if (player2.GetPoint() == 7)
			{
				if (this->score_saved == false)
				{
					MyFile.open("PongPoint", ios::app);
					MyFile << player2.GetName() << "has points: " << player2.GetPoint() << " Won at time: " << dt << " Game WON" << endl;
					MyFile.close();
					this->score_saved = true;

				}
				ball[index].Stop();
				window.clear(sf::Color(0, 0, 0));
				RemoveBricks();
				GameMessage.setFillColor(sf::Color::Black);
				sssScore << "Player 2 WON";
				Winner.setString(sssScore.str());
				window.draw(Winner);

				try_again_button->Show();
				try_again_button->Draw(window);
			}

			if (try_again_button->IsPressed())
			{
				//player.ResetScore();
				////ResetGame(window, ball);
				//try_again_button->Hide();
				//ball[index].Move();
				player1.ResetScore();
				player2.ResetScore();

				ResetGame(window, ball, player_paddle, enemy_paddle);

				try_again_button->Hide();
			}

			if (this->m_multiplayer)
				enemy_paddle.MovePaddle("Pong");
			else
				enemy_paddle.SelfMove(ball[index]);

			ball[index].Draw(window);

			player_paddle.Draw(window);
			player_paddle.CheckWallCollision(window);
			player_paddle.MovePaddle("Pong");
			player_paddle.FixWallCollision(window);

			enemy_paddle.Draw(window);
			enemy_paddle.CheckWallCollision(window);

			enemy_paddle.FixWallCollision(window);

			window.display();
		}
	}
}

void PongGame::SetGameConditions()
{
}
