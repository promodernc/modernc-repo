#include "Ball.h"

Ball::Ball()
{
	this->m_ball.setOutlineThickness(4);
	this->m_ball.setOutlineColor(sf::Color::Black);
	this->m_ball.setFillColor(sf::Color::Yellow);
	this->m_ball.setOrigin(16.0 / 2, 16.0 / 2);
	this->m_ball.setPosition(500, 500);
	this->m_ball.setRadius(16.0 - 4);
	this->m_speed = 30;
	this->is_moving = true;

}

void Ball::Hide()
{
	this->m_ball.setRadius(0);
}

void Ball::Show()
{
	this->m_ball.setRadius(16.0 - 4);
}

void Ball::Draw(sf::RenderWindow& window)
{
	window.draw(m_ball);
}

sf::Vector2f Ball::GetPosition() const
{
	return m_ball.getPosition();
}

void Ball::Move()
{
	this->is_moving = true;
}

void Ball::Stop()
{
	this->is_moving = false;
}

bool Ball::IsMoving()
{
	return this->is_moving;
}

float Ball::GetRadius() const
{
	return m_ball.getRadius();
}


void Ball::SetPosition(sf::Vector2f position)
{
	m_ball.setPosition(position);
}

void Ball::SetColor(sf::Color color)
{
	m_ball.setFillColor(color);
}

void Ball::SetThiccness(int thick)
{
	m_ball.setOutlineThickness(thick);
}

float Ball::GetVelocity(sf::Vector2f direction) const
{
	return std::sqrt(direction.x * direction.x + direction.y * direction.y);
}

void Ball::SetSpeed(float speed)
{
	m_speed = speed;
}

float Ball::GetSpeed() const
{
	return m_speed;
}