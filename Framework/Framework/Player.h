#pragma once
#include <SFML\Graphics.hpp>
#include <sstream>


using namespace std;

class Player
{
public:
	Player(std::string name, int matchesPlayed, int matchesWon, int matchesLost);
	

	const std::string& GetName() const;
	const int& GetMatchesPlayed() const;
	const int& GetMatchesWon() const;
	const int& GetMatchesLost() const;
	const sf::Text GetScore() const;
	const unsigned int GetPoint() const;
	
	void IncreaseScore();
	void ResetScore();
	void SetScorePosition(sf::Vector2f pozitie);

private:
	
	std::string m_name;
	unsigned int m_score;
	uint8_t m_matchesPlayed;
	uint8_t m_matchesWon;
	uint8_t m_matchesLost;
	sf::Font text_font;
	sf::Text lblScore;
	std::ostringstream ssScore;
};

