#pragma once
#include <SFML/Graphics.hpp>
#include "Ball.h"
#include "Player.h"
#include <random>
#include <vector>
#include"StateType.h"

class Brick
{
public:
	Brick(sf::Vector2f pozitie);
	
   CollisionSide CheckBallCollision(Ball ball);

	float GetX() const;
	float GetY() const;
	sf::Vector2f GetSize() const;

	void SetPosition(sf::Vector2f pozition);
	void SetColor(sf::Color color);

	void draw(sf::RenderWindow& window);

	bool OkToAddBrick(Brick* b, vector<Brick*> bricks);
	void GenerateBricks(vector<Brick*>& bricks, int nr,std::string game);
	void RemoveBricks(vector<Brick*>& bricks);
	void DrawBricks(sf::RenderWindow& window, vector<Brick*> bricks);

private:
	sf::RectangleShape brick;

};
