#pragma once
#include<SFML/Graphics.hpp>

class Ball
{
public:
	Ball();
	
	sf::Vector2f GetPosition() const ;
	
	float GetRadius() const;
	float GetVelocity(sf::Vector2f direction) const;
	float GetSpeed() const;

	void SetPosition(sf::Vector2f position);
	void SetColor(sf::Color color);
	void SetThiccness(int);
	void SetSpeed(float speed);

	void Draw(sf::RenderWindow& window);
	void Hide();
	void Show();

	void Move();
	void Stop();
	bool IsMoving();

private:
	sf::CircleShape m_ball;
	float m_speed;
	bool is_moving;
};
