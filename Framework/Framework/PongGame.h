#pragma once
#include"MyGame.h"
#include"SFML\Graphics.hpp"
#include"Ball.h"
#include"vector"
#include"Paddle.h"
#include"Brick.h"
#include"Player.h"
#include<vector>

class PongGame :public MyGame
{
public:
	PongGame(bool multiplayer);
	void SetObjectAndPlayer() override;
	void SetGameConditions() override;
	void ResetGame(sf::RenderWindow& window, std::vector<Ball>& ball, Paddle player1, Paddle player2);
	void RemoveBricks();
	bool score_saved;

private:
	std::vector<Brick*> bricks;
	Brick* brick;
	int nrbrick;
	bool m_multiplayer;

};

