#include "Paddle.h"

Paddle::Paddle()
{
	this->paddle.setFillColor(sf::Color::Red);
	this->paddle.setOutlineColor(sf::Color::Red);
	this->paddle.setOutlineThickness(3);
}

Paddle::Paddle(sf::Vector2f position, sf::Keyboard::Key move_up, sf::Keyboard::Key move_down, sf::Keyboard::Key move_left, sf::Keyboard::Key move_right)
{
	this->paddle.setFillColor(sf::Color::Red);
	this->paddle.setOutlineColor(sf::Color::Red);
	this->paddle.setOutlineThickness(3);
	this->paddle.setPosition(position);
	this->move_up = move_up;
	this->move_down = move_down;
	this->move_left = move_left;
	this->move_right = move_right;
}

void Paddle::Move(sf::Vector2f dir)
{
	paddle.move(dir);
}

void Paddle::SetSpeed(float speed)
{
	movespeed = speed;
}

float Paddle::GetSpeed() const
{
	return movespeed;
}

float Paddle::GetX() const
{
	return paddle.getPosition().x;
}

float Paddle::GetY() const
{
	return paddle.getPosition().y;
}

uint16_t Paddle::GetWidth() const
{
	return paddle.getLocalBounds().width;
}

uint16_t Paddle::GetHeight() const
{
	return paddle.getLocalBounds().height;
}

void Paddle::SetSize(sf::Vector2f size)
{
	paddle.setSize(size);
}

void Paddle::SetPosition(sf::Vector2f position)
{
	this->paddle.setPosition(position);
}

StateType Paddle::CheckBallLocation(Ball ball)
{
	StateType state = StateType::None;
	if (ball.GetPosition().y - ball.GetRadius() > GetY())
		state = StateType::Below;
	else if (ball.GetPosition().y + ball.GetRadius() < GetY() + GetHeight())
		state = StateType::Above;
	return state;
}

void Paddle::SelfMove(Ball ball)
{
	float speed = 0.2;
	switch (CheckBallLocation(ball))
	{
	case StateType::Below:
		paddle.setPosition(sf::Vector2f(GetX(), GetY() + speed));
		break;
	case StateType::Above:
		paddle.setPosition(sf::Vector2f(GetX(), GetY() -speed));
		break;
	default:
		break;
	}
}

void Paddle::Draw(sf::RenderWindow& window)
{
	window.draw(paddle);
}

void Paddle::SetThiccness(int thick)
{
	paddle.setOutlineThickness(thick);
}

StateType Paddle::CheckWallCollision(sf::RenderWindow& window)
{
	StateType state = StateType::None;
	//Left Wall 
	if (paddle.getPosition().x <= 0) {
		state = StateType::LeftSide;

	}
	//Top Wall
	if (paddle.getPosition().y <= 0) {
		state = StateType::UpperSide;

	}
	//Right Wall
	if (paddle.getPosition().x + paddle.getLocalBounds().width >= window.getSize().x) {
		state = StateType::RightSide;

	}
	//Bottom Wall
	if (paddle.getPosition().y + paddle.getLocalBounds().height >= window.getSize().y) {
		state = StateType::BottomSide;

	}
	return state;
}

void Paddle::FixWallCollision(sf::RenderWindow& window)
{
	switch (CheckWallCollision(window))
	{
	case StateType::LeftSide:
		paddle.setPosition(sf::Vector2f(0, paddle.getPosition().y));
		break;
	case StateType::UpperSide:
		paddle.setPosition(sf::Vector2f(paddle.getPosition().x, 0));
		break;
	case StateType::RightSide:
		paddle.setPosition(sf::Vector2f(window.getSize().x - paddle.getLocalBounds().width, paddle.getPosition().y));
		break;
	case StateType::BottomSide:
		paddle.setPosition(sf::Vector2f(paddle.getPosition().x, window.getSize().y - paddle.getLocalBounds().height));
		break;
	default: break;
	}
}

void Paddle::FixPongBallCollisionEnemy(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction)
{
	if (CheckPongBallCollisionEnemy(ball, new_pos, direction))
	{
		direction.x *= -1;
		new_pos.x = GetX() - ball.GetRadius();
	}
	
}
void Paddle::FixPongBallCollisionPlayer(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction)
{
	if (CheckPongBallCollisionPlayer(ball, new_pos, direction))
	{
		direction.x *= -1;
		new_pos.x = GetX() + ball.GetRadius();
	}

}
void Paddle::FixBrickBreakerBallCollision(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction)
{
	switch (CheckBrickBreakerBallCollision(ball, new_pos, direction))
	{
	case StateType::UpperSide:
		direction.y *= -1;
		new_pos.y = GetY() + ball.GetRadius();
		break;
	default:
		break;
	}
}

void Paddle::MovePaddle(std::string game)
{

	 
	if (game == "Pong")
	{
		if (sf::Keyboard::isKeyPressed(this->move_up)) {
			Move(sf::Vector2f(0, -movespeed));
		}

		if (sf::Keyboard::isKeyPressed(this->move_down)) {
			Move(sf::Vector2f(0, movespeed));
		}
	}
	else
	{
		if (game == "Brick")
		{
			if (sf::Keyboard::isKeyPressed(this->move_right)) {
				Move(sf::Vector2f(movespeed, 0));
			}
			if (sf::Keyboard::isKeyPressed(this->move_left)) {
				Move(sf::Vector2f(-movespeed, 0));
			}
		}
	}
}

bool Paddle::CheckPongBallCollisionEnemy(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction)
{
	if (new_pos.x - ball.GetRadius() < GetX()
		&& new_pos.x > GetX()
		&& new_pos.y > GetY()
		&& new_pos.y < GetY() + GetHeight())
	{
		return true;
	}

	return false;
}

bool Paddle::CheckPongBallCollisionPlayer(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction)
{
	if (new_pos.x + ball.GetRadius() > GetX() + GetWidth()
		&& new_pos.x - ball.GetRadius() < GetX()
		&& new_pos.y > GetY()
		&& new_pos.y < GetY() + GetHeight())
	{
		return true;
	}

	return false;
}

StateType Paddle::CheckBrickBreakerBallCollision(Ball& ball, sf::Vector2f& new_pos, sf::Vector2f& direction)
{
	StateType state=StateType::None;
	if (new_pos.y + ball.GetRadius() > GetY()
		&& new_pos.y + ball.GetRadius() > GetY() + GetHeight()
		&& new_pos.x > GetX()
		&& new_pos.x < GetX() + GetWidth())
	{
		state = StateType::UpperSide;
	}

	return state;
}