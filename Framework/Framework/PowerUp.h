#pragma once
#include"Brick.h"
#include"Paddle.h"
class PowerUp
{
public:
	void SizeUp(Paddle& paddle);
	void SizeDown(Paddle& paddle);
	void SizeUpBrickBreaker(Paddle& paddle);
	void SizeDownBrickBreaker(Paddle& paddle);
	void SpeedUpPaddle(Paddle& Paddle);
	void SpeedUp(Ball& ball);
	void SpeedDown(Ball& ball);
	void ChangeBrickToBall(std::vector<Ball>& vector, Ball& ball, Brick* brick, sf::RenderWindow& window);
	void DrawPowerUpSpeed(sf::CircleShape& power, Brick* brick, sf::RenderWindow& window);
};