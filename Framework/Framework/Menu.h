#pragma once
#include "SFML/Graphics.hpp"
#include "..\Button.h"



class Menu
{
public:
	Menu(float width, float height);
	~Menu();

	void Draw(sf::RenderWindow &window);


	void update(sf::Vector2f mousePos);

	int GetPressedItem();

	void UpdateMousePositions(sf::RenderWindow& window, sf::Vector2f& mousePos);

protected:
	sf::Vector2i mouse_pos_screen;
	sf::Vector2i mouse_pos_window;
	sf::Vector2f mouse_pos_view;

private:
	
	sf::Text text;
	int selectedItemIndex;
	sf::Font font;
	Button* play_brick_button;
	Button* play_pong_sp_button;
	Button* exit_button;
	Button* play_pong_mp_button;
	sf::Mouse mouse;

};