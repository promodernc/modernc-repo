#include"BrickBreakerGame.h"
#include"Ball.h"
#include"Brick.h"
#include"Paddle.h"
#include"Player.h"
#include"..\Button.h"
#include"PowerUp.h"

#include<iostream>
#include<fstream>
#include<ctime>
#include<sstream>
#include<random>
#include<vector>

BrickGame::BrickGame()
{
	this->brick = new Brick(sf::Vector2f(20, 20));
	this->brick->SetColor(sf::Color(rand() % 255 + 1, rand() % 255 + 1, rand() % 255 + 1, 255));
	this->brick->GenerateBricks(bricks, 20, "Brick");
	this->nrbrick = 20;
	this->lives = 3;
	this->gameover = 0;
	this->score_saved = false;
}

void BrickGame::ResetGame(sf::RenderWindow& window, std::vector<Ball>& ball)
{
	this->score_saved = false;
	this->gameover = 0;
	window.clear();
	this->brick->RemoveBricks(bricks);
	this->brick->GenerateBricks(bricks, 20, "Brick");
	this->nrbrick = 20;
	this->lives = 3;
	for (int index = 0; index < ball.size(); index++) {
		ball[index].Show();
		ball[index].SetSpeed(15);
		ball[index].SetPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));
	}
}

void BrickGame::SetObjectAndPlayer()
{
	srand(time(NULL));
	sf::RenderWindow window;
	sf::Vector2i centerWindow((sf::VideoMode::getDesktopMode().width / 2) - 445, (sf::VideoMode::getDesktopMode().height / 2) - 480);
	window.create(sf::VideoMode(900, 900), "Brick Breaker", sf::Style::Titlebar | sf::Style::Close);
	window.setPosition(centerWindow);
	window.setKeyRepeatEnabled(true);

	//***Defining Objects***//
	//file
	ofstream MyFile;

	
	
	//Mouse

	sf::Mouse mouse;
	sf::Vector2f mouse_position_window;

	//Paddle

	Paddle paddle(sf::Vector2f(450, 850), sf::Keyboard::W, sf::Keyboard::S, sf::Keyboard::A, sf::Keyboard::D);
	paddle.SetSize(sf::Vector2f(100, 0));
	paddle.SetSpeed(0.9);

	// Ball

	std::vector<Ball> ball;
	Ball first;
	ball.push_back(first);

	// Declaring Players

	Player player("George", 1, 1, 1);


	//Setting values, for moving the ball

	std::vector<sf::Vector2f> direction;
	sf::Vector2f dir(5, 15);
	direction.push_back(dir);
	sf::Clock clock;
	sf::Time elapsed = clock.restart();
	const sf::Time update_ms = sf::seconds(1.f / 90.f);

	// Fonts

	sf::Font agency;
	agency.loadFromFile("AGENCYB.ttf");

	sf::Font arial;
	arial.loadFromFile("arial.ttf");

	//Text for lives
	int lives = 3;

	sf::Text Lives;
	Lives.setCharacterSize(30);
	Lives.setPosition({ 400,850 });
	Lives.setFont(arial);
	Lives.setFillColor(sf::Color::Magenta);


	PowerUp p;

	Button* try_again_button = new Button(sf::Vector2f(window.getSize().x / 2 - 75, window.getSize().y / 2), 150, 90, arial, "Try again", sf::Color::Green, sf::Color::White, sf::Color::Red);

	while (window.isOpen())
	{
		//Time 

		time_t now = time(0);
		time_t result = time(NULL);
		char dt[26];
		ctime_s(dt, sizeof dt, &result);

		// Optimize mouse position for window
		mouse_position_window = window.mapPixelToCoords(sf::Mouse::getPosition(window));
		try_again_button->Update(mouse_position_window);

		//Text for setting Win or Lose

		sf::Text GameMessage;
		GameMessage.setCharacterSize(100);
		GameMessage.setPosition({ 250,250 });
		GameMessage.setFont(agency);
		GameMessage.setFillColor(sf::Color::Red);

		//Event Loop:

		sf::Event Event;

		while (window.pollEvent(Event)) {

			switch (Event.type) {

			case sf::Event::Closed:
				window.close();
			}

		}
		
		for (int index = 0; index < ball.size(); index++) {
			elapsed += clock.restart();
			if (ball[index].IsMoving())
			{
				while ((elapsed >= update_ms) && (this->lives != 0 && player.GetPoint() != nrbrick)) {
					const auto pos = ball[index].GetPosition();
					const auto delta = update_ms.asSeconds() * ball[index].GetSpeed();
					sf::Vector2f new_pos(pos.x + direction[index].x * delta, pos.y + direction[index].y * delta);

					if (new_pos.x - ball[index].GetRadius() < 0) { // left window edge
						direction[index].x *= -1;
						new_pos.x = 0 + ball[index].GetRadius();
						

					}
					else if (new_pos.x + ball[index].GetRadius() >= window.getSize().x) { // right window edge
						direction[index].x *= -1;
						new_pos.x = window.getSize().x - ball[index].GetRadius();


					}
					else if (new_pos.y - ball[index].GetRadius() < 0) { // top of window
						direction[index].y *= -1;
						new_pos.y = 0 + ball[index].GetRadius();

					}
					else if (new_pos.y + ball[index].GetRadius() >= window.getSize().y)
					{ // bottom of window
						direction[index].y *= -1;
						new_pos.x = 450;
						new_pos.y = 450;
						ball[index].SetPosition(new_pos);
						//new_pos.y = 500;
						this->lives--;
						ball[index].Stop();

						sf::Text life_lost_text;
						std::ostringstream text;
						text << "1 Life Lost!";
						life_lost_text.setCharacterSize(30);
						life_lost_text.setPosition({ 360, 550 });
						life_lost_text.setFont(arial);
						life_lost_text.setFillColor(sf::Color::Red);
						life_lost_text.setString(text.str());
						window.draw(life_lost_text);
						window.display();

						sf::Time wait_time = clock.getElapsedTime();
						while (clock.getElapsedTime().asSeconds() < wait_time.asSeconds() + 2);

						text.str("");
						life_lost_text.setString(text.str());

						ball[index].SetSpeed(15);//speed reduction for easier restart

						paddle.SetSize(sf::Vector2f(100, 0));//reset the paddle if was bigger
						paddle.SetSpeed(0.9);//Reset the speed of paddle
					}
					ball[index].SetPosition(new_pos);
					ball[index].Move();
					paddle.FixBrickBreakerBallCollision(ball[index], new_pos, direction[index]);
					elapsed -= update_ms;
				}
			}

			window.clear(sf::Color(0, 0, 0));

			//update lives

			std::ostringstream ssScore;
			ssScore << "Lives " << this->lives;
			Lives.setString(ssScore.str());
			Lives.setString(ssScore.str());
			window.draw(Lives);

			//Displaying the values and objects


			if (this->lives == 0)
			{
				ssScore.str("");

				this->gameover = 2;

				ssScore << "GAME OVER ";

				if (this->score_saved == false)
				{
					ctime_s(dt, sizeof dt, &result);
					MyFile.open("BrickPoint", ios::app);
					MyFile << "Game point: " << player.GetPoint() << ", At time " << dt << " Game LOST" << endl;
					MyFile.close();
					this->score_saved = true;
				}

				ball[index].Stop();
				GameMessage.setString(ssScore.str());
				window.draw(GameMessage);
				//return;

				RemoveBricks();
				try_again_button->Show();
				try_again_button->Draw(window);

			}

			if (player.GetPoint() == 20)
			{

				ssScore.str("");

				this->gameover = 1;

				ssScore << "GAME WON ";
				if (this->score_saved == false)
				{
					ctime_s(dt, sizeof dt, &result);
					MyFile.open("BrickPoint", ios::app);
					MyFile << "Game point: " << player.GetPoint() << ", At time " << dt << " Game WON" << endl;
					MyFile.close();
					this->score_saved = true;
				}
				ball[index].Stop();
				GameMessage.setString(ssScore.str());
				window.draw(GameMessage);

				try_again_button->Show();
				try_again_button->Draw(window);
			}

			if (try_again_button->IsPressed())
			{
				player.ResetScore();
				ResetGame(window, ball);
				try_again_button->Hide();
				ball[index].SetPosition(sf::Vector2f(500, 500));
				ball[index].Move();
			}

			//Score
			window.draw(player.GetScore());

			//Ball and Paddle
			ball[index].Draw(window);

			paddle.Draw(window);
			paddle.CheckWallCollision(window);
			paddle.MovePaddle("Brick");
			paddle.FixWallCollision(window);

			this->brick->DrawBricks(window, this->bricks);
			for (Brick* b : this->bricks)
			{
				if ((b->CheckBallCollision(ball[index]) == (CollisionSide::BottomAndTopSide))
					|| (b->CheckBallCollision(ball[index]) == CollisionSide::LeftAndRightSide))
				{
					direction[index].y *= -1;
					ball[index].SetSpeed(ball[index].GetSpeed() + 2); //increasing speed after collision
					int powerup = 0;
					srand(time(NULL));
					powerup = rand() % 100 + 1;
					if (powerup % 2 == 0)
					{
						p.SizeUpBrickBreaker(paddle);
						p.SpeedUpPaddle(paddle);
					}


					if (powerup % 2 == 1)
						p.SpeedUp(ball[index]);

					if (powerup % 200 == 0)
					{
						p.ChangeBrickToBall(ball, first, b, window);
						direction.push_back(dir);
					}
					b->SetPosition(sf::Vector2f(901, 901));
					player.IncreaseScore();
				}
			}
			window.display();
		}
	}
	//if (score_saved == false)
	//{
	//	if (this->gameover == 2)
	//	{
	//		MyFile.open("BrickPoint", ios::app);
	//		MyFile << "Game point: " << player.GetPoint() << ", At time " << dt << " Game LOST" << endl;
	//		MyFile.close();
	//	}
	//	if (this->gameover == 1)
	//	{
	//		MyFile.open("BrickPoint", ios::app);
	//		MyFile << "Game point: " << player.GetPoint() << ", At time " << dt << " Game WON" << endl;
	//		MyFile.close();
	//	}
	//	score_saved = true;
	//}
}

void BrickGame::RemoveBricks()
{
	this->brick->RemoveBricks(this->bricks);
}

void BrickGame::SetGameConditions()
{
}
