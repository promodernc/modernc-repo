#include"BrickBreakerGame.h"
#include"PongGame.h"
#include"Menu.h"

int main() {
	
	//BrickGame* BrickGameplay=new BrickGame();
	//PongGame* Pongplay=new PongGame();
	//
	//MyGame* maingame= new BrickGame();
	////MyGame* maingame = BrickGameplay;
	//
	//maingame->SetObjectAndPlayer();
	////Game2.SetObjectAndPlayer();
	sf::RenderWindow window(sf::VideoMode(600, 600), "SFML WORK!");
	sf::Text text;
	sf::Font vegan;
	vegan.loadFromFile("Vegan.ttf");
	text.setFont(vegan);
	text.setCharacterSize(30);
	text.setPosition({170,50 });
	text.setCharacterSize(40);
	text.setFillColor(sf::Color(255, 69, 0));
	text.setString("Ball Framework");

	Menu menu(window.getSize().x, window.getSize().y);
	//window.draw(text);
	sf::Mouse mouse;
	sf::Vector2f mousePos(mouse.getPosition().x, mouse.getPosition().y);

	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (menu.GetPressedItem())
			{
			case 0:
			{
				MyGame* maingame = new BrickGame();
				maingame->SetObjectAndPlayer();
				break;
			}
			case 1:
			{
				MyGame* maingame = new PongGame(false);
				maingame->SetObjectAndPlayer();
				break;
			}
			case 2:
			{
				MyGame* maingame = new PongGame(true);
				maingame->SetObjectAndPlayer();
				break;
			}
			case 3:
				window.close();
				break;
			case -1:
				break;
			}

			if (event.type == sf::Event::Closed)
				window.close();

			window.clear(sf::Color(240, 248, 255));

			menu.UpdateMousePositions(window, mousePos);

			menu.update(mousePos);

			menu.Draw(window);
			window.draw(text);
			window.display();
		}
	}
}