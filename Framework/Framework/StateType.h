#pragma once
#include <string>
#include <cstdint>
enum class StateType :uint8_t
{
	//daca loveste sus,jos,dreapta,stanga
	//folosim pentru fixarea paletei, daca iese din fereastra,precum si pentru coliziounea mingii cu paleta
	UpperSide,
	BottomSide,
	RightSide,
	LeftSide,
	Above,
	Below,
	None
};
enum class CollisionSide :uint8_t
{
	LeftAndRightSide,
	BottomAndTopSide
};

inline StateType ConvertStringToStateType(const std::string type)
{
	if (type == "UpperSide")
		return StateType::UpperSide;
	else if (type == "BottomSide")
		return StateType::BottomSide;
	else if (type == "RightSide")
		return StateType::RightSide;
	else if (type == "LeftSide")
		return StateType::LeftSide;
	else if (type == "Above")
		return StateType::Above;
	else if (type == "Below")
		return StateType::Below;
	else if (type == "None")
		return StateType::None;

	throw "Invalid type!";
}
