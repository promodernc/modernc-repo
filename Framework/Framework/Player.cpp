#include "Player.h"

Player::Player(std::string name, int matchesPlayed, int matchesWon, int matchesLost)
	:m_name(name), m_score(0), m_matchesPlayed(matchesPlayed), m_matchesWon(matchesWon), m_matchesLost(matchesLost)
{
	text_font.loadFromFile("AGENCYB.TTF");

	ssScore << "Score : " << m_score;

	lblScore.setCharacterSize(30);
	lblScore.setFont(text_font);
	lblScore.setPosition(10, 800);
	lblScore.setString(ssScore.str());
	lblScore.setFillColor(sf::Color(255, 140, 0));
}


const std::string& Player::GetName() const
{
	return m_name;
}

const int& Player::GetMatchesWon() const
{
	return m_matchesWon;
}

const int& Player::GetMatchesLost() const
{
	return m_matchesLost;
}

const int& Player::GetMatchesPlayed() const
{
	return m_matchesPlayed;
}

const sf::Text Player::GetScore() const
{
	return lblScore;
}

const unsigned int Player::GetPoint() const
{
	return m_score;
}

void Player::IncreaseScore()
{
	m_score++;
	ssScore.str("");
	ssScore << "Score : " << m_score;
	lblScore.setString(ssScore.str());
}

void Player::ResetScore()
{
	m_score = 0;
	ssScore.str("");
	ssScore << "Score : " << m_score;
	lblScore.setString(ssScore.str());
}

void Player::SetScorePosition(sf::Vector2f pozitie)
{
	lblScore.setPosition(pozitie);
}
